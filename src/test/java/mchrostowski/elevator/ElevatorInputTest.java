package mchrostowski.elevator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

public class ElevatorInputTest {

	private static final String EXCEPTION_STARTING_INPUT = "exceptionalStartingInput";
	private static final String EXCEPTION_CMD_INPUT1 = "exceptionalCommandInput1";
	private static final String EXCEPTION_CMD_INPUT2 = "exceptionalCommandInput2";

	@Test(expected=IllegalArgumentException.class)
	public void exceptionalStartingInput() throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(
				Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(EXCEPTION_STARTING_INPUT)));
		String line = r.readLine();
		@SuppressWarnings("unused") // expected exception to be thrown here
		ElevatorInput eIn = ElevatorInput.valueOf(line);
	}

	@Test(expected=IllegalArgumentException.class)
	public void exceptionalCommandInput1() throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(
				Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(EXCEPTION_CMD_INPUT1)));
		String line = r.readLine();
		@SuppressWarnings("unused") // expected exception to be thrown here
		ElevatorInput eIn = ElevatorInput.valueOf(line);
	}

	@Test(expected=IllegalArgumentException.class)
	public void exceptionalCommandInput2() throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(
				Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(EXCEPTION_CMD_INPUT2)));
		String line = r.readLine();
		@SuppressWarnings("unused") // expected exception to be thrown here
		ElevatorInput eIn = ElevatorInput.valueOf(line);
	}
}
