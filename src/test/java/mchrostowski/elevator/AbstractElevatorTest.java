package mchrostowski.elevator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * Instantiates an {@link Elevator} by using the abstract method
 * getElevatorInstance() which is then tested against the data in the 
 * test/resources/testInput file. Results are compared against the file located 
 * at test/resource/{resultFilename} where resultFilename is gotten from the 
 * return value of getResultsFilename().
 */
public abstract class AbstractElevatorTest {

	protected abstract Elevator getElevatorInstance();

	protected abstract String getResultsFilename();

	private static final String TEST_INPUT_FILENAME = "testInput";
	
	private static final int TEST_INPUT_LINE_COUNT = 6;

	/**
	 * Extracts a set of expected results from a file.
	 * 
	 * @param filename
	 *            name of the file to be loaded
	 * @return a {@link List} of {@link List}s of floors at which we expect the
	 *         elevator to stop, each list corresponds to a single
	 *         {@link ElevatorInput}
	 * @throws IOException
	 */
	protected static List<List<Integer>> populateExpectedResults(String filename) throws IOException {
		List<List<Integer>> expectedResults = new ArrayList<>(TEST_INPUT_LINE_COUNT);
		BufferedReader r = new BufferedReader(new InputStreamReader(
				Thread.currentThread().getContextClassLoader().getResourceAsStream(filename)));
		String line;
		while ((line = r.readLine()) != null) {
			String[] floorsAndCount = line.split(" "); // Count is always last
			List<Integer> floors = new ArrayList<>(floorsAndCount.length);
			for (int i = 0; i < floorsAndCount.length-1; ++i) { //ignore Count field
				floors.add(Integer.valueOf(floorsAndCount[i]));
			}
			expectedResults.add(floors);
		}
		r.close();
		return expectedResults;
	}

	@Test
	public void inputFile() throws IOException {
		List<List<Integer>> expectedResults = populateExpectedResults(getResultsFilename());
		BufferedReader r = new BufferedReader(new InputStreamReader(
				Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(TEST_INPUT_FILENAME)));
		String line;
		Iterator<List<Integer>> expectedResultsItr = expectedResults.iterator();
		while ((line = r.readLine()) != null) {
			ElevatorInput input = ElevatorInput.valueOf(line);
			Elevator elevator = getElevatorInstance();
			List<Integer> elevatorStops = elevator.determineStops(input);
			if (elevatorStops == null || elevatorStops.isEmpty()) {
				Assert.fail("Elevator output missing! " + elevatorStops);
			}
			if (!expectedResultsItr.hasNext()) {
				Assert.fail("Expected output underflow or elevator output overflow.");
			}
			List<Integer> expectedStops = expectedResultsItr.next();
			Assert.assertEquals("Expected and actual stops do not match for input:\n" + line,
								expectedStops, elevatorStops);
		}
		r.close();
	}
	
}