package mchrostowski.elevator;

public class ModeBElevatorTest extends AbstractElevatorTest {

	@Override
	protected Elevator getElevatorInstance() {
		return new ModeBElevator();
	}

	@Override
	protected String getResultsFilename() {
		return "modeBExpectedOutput";
	}

}
