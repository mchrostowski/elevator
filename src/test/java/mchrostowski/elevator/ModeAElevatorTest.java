package mchrostowski.elevator;

public class ModeAElevatorTest extends AbstractElevatorTest {

	@Override
	protected String getResultsFilename() {
		return "modeAExpectedOutput";
	}
	
	@Override
	protected Elevator getElevatorInstance() {
		return new ModeAElevator();
	}

}