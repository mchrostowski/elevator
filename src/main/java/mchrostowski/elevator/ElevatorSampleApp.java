package mchrostowski.elevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElevatorSampleApp {
	
	private static final Logger log = LoggerFactory.getLogger(ElevatorSampleApp.class);

	private static final int INPUT_FILE_ARG = 0;
	private static final int MODE_ARG = 1;
	
	private enum Mode {
		A, B;
	}
	
	// TODO should be spring-ified should this application grow larger
	// (specifically if more elevator types are added)
	public static void main(String[] args) {
		if (args.length != 2) {
			printUsageAndExit();
		}
		
		final String filename = args[INPUT_FILE_ARG];
		final File inputFile = new File(filename);
		log.trace("Using input file: " + inputFile);
		
		final String modeStr = args[MODE_ARG];
		final Elevator elevator = getElevator(modeStr);
		
		try {
			consumeAndPrintInputFile(inputFile, elevator);
		} catch (IOException e) {
			log.error("Error reading file: " + filename, e);
		}
	}

	private static void printUsageAndExit() {
		System.out.println("Usage: elevator-sample <input file> <mode-a|mode-b>");
		System.exit(1);
	}

	private static Elevator getElevator(final String modeStr) {
		Mode mode;
		switch(modeStr) {
		case "mode-a":
			mode = Mode.A;
			break;
		case "mode-b":
			mode = Mode.B;
			break;
		default:
			log.error("Invalid mode specified: " + modeStr);
			mode = null; // squelch assignment warning, since we're about to System.exit
			printUsageAndExit();
		}
		log.trace("Using elevator mode: " + mode);
		
		final Elevator elevator;
		switch (mode) {
		case A:
			elevator = new ModeAElevator();
			break;
		case B:
			elevator = new ModeBElevator();
			break;
		default:
			throw new RuntimeException("Unknown elevator mode: " + mode);
		}
		return elevator;
	}

	private static void consumeAndPrintInputFile(File inputFile, Elevator e) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(inputFile));
		String line;
		while ((line = br.readLine()) != null) {
			ElevatorInput input = ElevatorInput.valueOf(line);
			int totalDistance = 0;
			Integer previousStop = null;
			List<Integer> stops = e.determineStops(input);
			StringBuilder output = new StringBuilder();
			for (Integer stop : stops) {
				output.append(stop + " ");
				if (previousStop != null)
					totalDistance += Math.abs(previousStop - stop);
				previousStop = stop;
			}
			output.append("(" + totalDistance + ")");
			System.out.println(output);
		}
		br.close();
	}

}
