package mchrostowski.elevator;

import java.util.ArrayList;
import java.util.List;

/**
 * Separates each given {@link Command} in a single {@link ElevatorInput} into 
 * a separate trip. Effectively this {@link Elevator} stops at all floors in 
 * the same order in which it is commanded.
 */
public class ModeAElevator implements Elevator {

	@Override
	public List<Integer> determineStops(ElevatorInput input) {
		List<Integer> stops = new ArrayList<>();
		int currentFloor = input.getStartingFloor();
		stops.add(currentFloor);
		for (Command command : input.getCommands()) {
			final int startFloor = command.getStartFloor();
			if (startFloor != currentFloor) {
				stops.add(startFloor);
				currentFloor = startFloor;
			}
			final int endFloor = command.getEndFloor();
			if (endFloor != currentFloor) {
				stops.add(endFloor);
				currentFloor = endFloor;
			}
		}
		return stops;
	}

}
