package mchrostowski.elevator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * {@link Elevator} which merges all consecutive {@link Command}s that do not
 * require the elevator to change direction and are at an appropriate floor
 * relative to our current trip.
 * 
 * Fairly reduces the amount of trips as much as possible without having any
 * passengers ride in an unexpected direction (not the direction of their
 * intended trip).
 */
public class ModeBElevator implements Elevator {

	// allStops and consecutiveStops are final so we use clear() instead of new
	/**
	 * Tracks all of the stops necessary for the given input.
	 */
	private final List<Integer> allStops = new ArrayList<>();
	/**
	 * Tracks all the stops we'll be making in a single direction. These are
	 * merged together before being appended onto allStops.
	 */
	private final NavigableSet<Integer> consecutiveStops = new TreeSet<>();

	private enum Direction {
		UP, DOWN, NONE;
	}

	@Override
	public List<Integer> determineStops(ElevatorInput input) {
		allStops.clear();
		consecutiveStops.clear();
		/**
		 * Tracks the direction of our current Set of consecutiveStops.
		 */
		Direction currentDirection;
		{
			Command cmd = input.getCommands().get(0);
			currentDirection = getDirection(cmd.getStartFloor(), cmd.getEndFloor());
		}
		consecutiveStops.add(input.getStartingFloor());
		completeConsecutiveStops(Direction.NONE);

		for (Command command : input.getCommands()) {

			final int startFloor = command.getStartFloor();
			final int endFloor = command.getEndFloor();

			Direction commandDir = getDirection(startFloor, endFloor);

			boolean endOfConsecutiveStops = false;
			switch (currentDirection) {
			case UP:
				if (!commandDir.equals(Direction.UP))
					endOfConsecutiveStops = true;
				break;
			case DOWN:
				if (!commandDir.equals(Direction.DOWN))
					endOfConsecutiveStops = true;
				break;
			case NONE:
				break;
			}
			if (endOfConsecutiveStops) {
				completeConsecutiveStops(currentDirection);
				currentDirection = commandDir;
			}
			consecutiveStops.add(startFloor);
			consecutiveStops.add(endFloor);
		}
		completeConsecutiveStops(currentDirection);
		return allStops;
	}
	
	/** TODO update this comment, no de-duplication is done
	 * Sorts and de-duplicates all {@link Integer}s in consecutiveStops and then
	 * moves the values onto the end of allStops. The member consecutiveStops is 
	 * left clear()'ed by this method.
	 */
	private void completeConsecutiveStops(Direction currentDirection) {
		Iterator<Integer> stopsItr;
		switch(currentDirection) {
		case UP:
			stopsItr = consecutiveStops.iterator();
			break;
		case DOWN:
			stopsItr = consecutiveStops.descendingIterator();
			break;
		default:
			if (consecutiveStops.size() > 1)
				throw new RuntimeException("Invalid direction: " + currentDirection
						+ "\nFor stops: " + consecutiveStops);
			stopsItr = consecutiveStops.iterator();
		}
		while (stopsItr.hasNext()) {
			final Integer nextStop = stopsItr.next();
			final Integer latestStop;
			if (allStops.size() > 0)
				latestStop = allStops.get(allStops.size()-1);
			else
				latestStop = null;
			if (!nextStop.equals(latestStop))
				allStops.add(nextStop);
		}
		consecutiveStops.clear();
	}

	private Direction getDirection(int fromFloor, int toFloor) {
		Direction d;
		if (fromFloor < toFloor)
			d = Direction.UP;
		else if (fromFloor > toFloor)
			d = Direction.DOWN;
		else
			d = Direction.NONE;
		return d;
	}

}
