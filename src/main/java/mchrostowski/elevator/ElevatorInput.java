package mchrostowski.elevator;

import java.util.LinkedList;
import java.util.List;

/**
 * A {@link List} of {@link Command}s along with an initial floor
 * for an {@link Elevator} to process.
 */
public class ElevatorInput {
	private static final int TOP_FLOOR = 12;

	private int startingFloor;
	private List<Command> commands;

	public ElevatorInput(int startingFloor, List<Command> commands) {
		this.startingFloor = startingFloor;
		this.commands = commands;
	}

	public int getStartingFloor() {
		return startingFloor;
	}
	public List<Command> getCommands() {
		return commands;
	}

	@Override
	public String toString() {
		return "ElevatorInput [" + startingFloor + ":" + commands + "]";
	}

	/**
	 * Takes strings in an appropriate format and parses them into an
	 * {@link ElevatorInput}.
	 */
	public static ElevatorInput valueOf(String input) {
		int startingFloor;
		String commandsStr;
		{
			String[] floorCommands = input.split(":");
			startingFloor = Integer.parseInt(floorCommands[0]);
			if (startingFloor > TOP_FLOOR)
				throw new IllegalArgumentException(
						"Initial floor past top floor (" + TOP_FLOOR + ") for input: " + input);
			commandsStr = floorCommands[1];
		}
		List<Command> commands = new LinkedList<>();
		for (String command : commandsStr.split(",")) {
			String[] levels = command.split("-");
			Command cmd = new Command(levels[0], levels[1]);
			if (cmd.getStartFloor() > TOP_FLOOR || cmd.getEndFloor() > TOP_FLOOR)
				throw new IllegalArgumentException(
						"Received command with invalid floors: " + cmd);
			commands.add(cmd);

		}
		return new ElevatorInput(startingFloor, commands);
	}
}