package mchrostowski.elevator;

import java.util.List;

public interface Elevator {

	/**
	 * Returns a list of floors at which the elevator will stop given the
	 * specified {@link ElevatorInput}.
	 *
	 * @param elevatorInput
	 * @return a {@link List} of {@link Integer}s representing the floors at
	 *         which the elevator will stop, includes the startingFloor
	 */
	List<Integer> determineStops(ElevatorInput input);

}
