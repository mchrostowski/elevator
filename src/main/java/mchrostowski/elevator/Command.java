package mchrostowski.elevator;

/**
 * Command for an Elevator to request transport from startFloor() to
 * endFloor().
 */
public class Command {

	private final int startFloor;
	private final int endFloor;

	public Command(int startFloor, int endFloor) {
		this.startFloor = startFloor;
		this.endFloor = endFloor;
	}

	/**
	 * @param startFloor
	 * @param endFloor
	 * @throws NumberFormatException
	 *             if startFloor or endFloor cannot be converted to Integer
	 */
	public Command(String startFloor, String endFloor) {
		this.startFloor = Integer.valueOf(startFloor);
		this.endFloor = Integer.valueOf(endFloor);
	}

	public int getStartFloor() {
		return startFloor;
	}

	public int getEndFloor() {
		return endFloor;
	}

	@Override
	public String toString() {
		return "Command [" + startFloor + "-" + endFloor + "]";
	}

}
